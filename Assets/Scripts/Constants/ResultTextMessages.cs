﻿namespace DefaultNamespace
{
    public static class ResultTextMessages
    {
        public const string FirstOperandIsEmpty = "Первый операнд не заполнен.";
        public const string SecondOperandIsEmpty = "Второй операнд не заполнен.";
        public const string ThirdOperandIsEmpty = "Третий операнд не заполнен.";
        public const string FirstOperandIsNotNumber = "Первый операнд не является числом.";
        public const string SecondOperandIsNotNumber = "Второй операнд не является числом.";
        public const string ThirdOperandIsNotNumber = "Третий операнд не является числом.";
        public const string FirstOperandIsZero = "Первый операнд не должен быть равен нулю.";
        public const string SecondOperandIsZero = "На ноль делить нельзя.";
        public const string UnknownMathOperation = "Неизвестная математическая операция.";
        public const string NumbersEquals = "Числа равны";
        public const string DiscriminantLessThanZero = "Корни отсутствуют.";
        public const string DiscriminantEqualsZero = "Корень равен ";
        public const string DiscriminantGreaterZhanZeroFirstRoot = "Первый корень равен ";
        public const string DiscriminantGreaterZhanZeroSecondRoot = "Второй корень равен ";
    }
}