using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NavbarButtonClickHandler : MonoBehaviour
{
    [SerializeField] private List<Button> navbarButtons;
    [SerializeField] private List<GameObject> layoutPanels;

    private Dictionary<Button, GameObject> _navbarButtonsToLayoutPanels;
    
    private void Start()
    {
        _navbarButtonsToLayoutPanels = new Dictionary<Button, GameObject>()
        {
            {navbarButtons[0],layoutPanels[0]},
            {navbarButtons[1],layoutPanels[1]},
            {navbarButtons[2],layoutPanels[2]},
            {navbarButtons[3],layoutPanels[3]}
        };
        
        navbarButtons.ForEach(button =>
        {
            button.onClick.AddListener(() =>
            {
                SetButtonsInteractableExclude(button);
                ActivateSelectedLayoutPanelAndDeactivateOthers(button);
            });
        });
    }

    private void SetButtonsInteractableExclude(Button excludeButton)
    {
        navbarButtons.ForEach(button => button.interactable = !button.Equals(excludeButton));
    }

    private void ActivateSelectedLayoutPanelAndDeactivateOthers(Button clickedNavbarButton)
    {
        var layoutPanelToActivate = _navbarButtonsToLayoutPanels[clickedNavbarButton];
        

        layoutPanels
            ?.ForEach(layoutPanel => layoutPanel.SetActive(layoutPanel.Equals(layoutPanelToActivate)));
    }
}