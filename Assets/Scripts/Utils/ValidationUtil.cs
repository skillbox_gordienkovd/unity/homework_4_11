﻿using static DefaultNamespace.ResultTextMessages;

namespace DefaultNamespace
{
    public static class ValidationUtil
    {
        public static bool ValidateOperandsIsNotEmpty(
            string firstOperandText,
            string secondOperandText,
            out string errorMessage
        )
        {
            if (string.IsNullOrEmpty(firstOperandText))
            {
                errorMessage = FirstOperandIsEmpty;
                return false;
            }

            if (string.IsNullOrEmpty(secondOperandText))
            {
                errorMessage = SecondOperandIsEmpty;
                return false;
            }

            errorMessage = null;

            return true;
        }
        
        public static bool ValidateOperandsIsNotEmpty(
            string firstOperandText,
            string secondOperandText,
            string thirdOperandText,
            out string errorMessage
        )
        {
            if (string.IsNullOrEmpty(firstOperandText))
            {
                errorMessage = FirstOperandIsEmpty;
                return false;
            }

            if (string.IsNullOrEmpty(secondOperandText))
            {
                errorMessage = SecondOperandIsEmpty;
                return false;
            }
            
            if (string.IsNullOrEmpty(thirdOperandText))
            {
                errorMessage = ThirdOperandIsEmpty;
                return false;
            }

            errorMessage = null;

            return true;
        }
    }
}