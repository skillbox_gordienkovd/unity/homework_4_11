﻿using System;
using static DefaultNamespace.ResultTextMessages;

namespace DefaultNamespace
{
    public static class ComparisonUtil
    {
        public static string ExecuteComparisonFindBigger(
            double firstOperandNumber,
            double secondOperandNumber
        )
        {
            string result;

            if (firstOperandNumber.Equals(secondOperandNumber))
            {
                result = NumbersEquals;
            }
            else
            {
                result = firstOperandNumber > secondOperandNumber
                    ? $"{firstOperandNumber}"
                    : $"{secondOperandNumber}";
            }

            return result;
        }

        public static string ExecuteComparisonFindTwoBiggest(
            double firstOperandNumber,
            double secondOperandNumber,
            double thirdOperandNumber
        )
        {
            string result;

            if (firstOperandNumber.Equals(secondOperandNumber) && firstOperandNumber.Equals(thirdOperandNumber))
            {
                result = NumbersEquals;
            }
            else
            {
                var minOperandNumber = Math.Min(Math.Min(firstOperandNumber, secondOperandNumber), thirdOperandNumber);

                if (minOperandNumber.Equals(firstOperandNumber))
                {
                    result = $"{secondOperandNumber} и {thirdOperandNumber}";
                }
                else if (minOperandNumber.Equals(secondOperandNumber))
                {
                    result = $"{firstOperandNumber} и {thirdOperandNumber}";
                }
                else
                {
                    result = $"{firstOperandNumber} и {secondOperandNumber}";
                }
            }

            return result;
        }
    }
}