﻿using static DefaultNamespace.ResultTextMessages;

namespace DefaultNamespace
{
    public static class ParseDoubleUtil
    {
        public static bool TryParseDoubleFromOperandsText(
            string firstOperandText,
            string secondOperandText,
            out double firstOperandNumber,
            out double secondOperandNumber,
            out string errorMessage
        )
        {
            var isSuccessParsedFirstOperandText = double.TryParse(firstOperandText, out firstOperandNumber);
            var isSuccessParsedSecondOperandText = double.TryParse(secondOperandText, out secondOperandNumber);

            if (!isSuccessParsedFirstOperandText)
            {
                errorMessage = FirstOperandIsNotNumber;

                return false;
            }

            if (!isSuccessParsedSecondOperandText)
            {
                errorMessage = SecondOperandIsNotNumber;

                return false;
            }

            errorMessage = null;

            return true;
        }
        
        public static bool TryParseDoubleFromOperandsText(
            string firstOperandText,
            string secondOperandText,
            string thirdOperandText,
            out double firstOperandNumber,
            out double secondOperandNumber,
            out double thirdOperandNumber,
            out string errorMessage
        )
        {
            var isSuccessParsedFirstOperandText = double.TryParse(firstOperandText, out firstOperandNumber);
            var isSuccessParsedSecondOperandText = double.TryParse(secondOperandText, out secondOperandNumber);
            var isSuccessParsedThirdOperandText = double.TryParse(thirdOperandText, out thirdOperandNumber);

            if (!isSuccessParsedFirstOperandText)
            {
                errorMessage = FirstOperandIsNotNumber;

                return false;
            }

            if (!isSuccessParsedSecondOperandText)
            {
                errorMessage = SecondOperandIsNotNumber;

                return false;
            }
            
            if (!isSuccessParsedThirdOperandText)
            {
                errorMessage = ThirdOperandIsNotNumber;

                return false;
            }

            errorMessage = null;

            return true;
        }
    }
}