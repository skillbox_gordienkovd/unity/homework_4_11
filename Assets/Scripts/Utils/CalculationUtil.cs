﻿using System;
using static DefaultNamespace.ResultTextMessages;

namespace DefaultNamespace
{
    public static class CalculationUtil
    {
        public static string ExecuteCalculateCommand(
            CalculateCommand command,
            double firstOperandNumber,
            double secondOperandNumber
        )
        {
            var result = command switch
            {
                CalculateCommand.Add => $"{firstOperandNumber + secondOperandNumber}",
                CalculateCommand.Subtract => $"{firstOperandNumber - secondOperandNumber}",
                CalculateCommand.Multiply => $"{firstOperandNumber * secondOperandNumber}",
                CalculateCommand.Divide => $"{firstOperandNumber / secondOperandNumber}",
                _ => UnknownMathOperation
            };

            return result;
        }

        public static string ExecuteSquareEquationDecision(
            double firstOperandNumber,
            double secondOperandNumber,
            double thirdOperandNumber
        )
        {
            string result;
            
            var discriminant = secondOperandNumber * secondOperandNumber
                               - 4 * firstOperandNumber * thirdOperandNumber;

            if (discriminant < 0)
            {
                result = DiscriminantLessThanZero;
            } else if (discriminant == 0)
            {
                result = DiscriminantEqualsZero + $"{-secondOperandNumber / (2 * firstOperandNumber)}.";
            }
            else
            {
                result = DiscriminantGreaterZhanZeroFirstRoot +
                         $"{(-secondOperandNumber - Math.Sqrt(discriminant)) / (2 * firstOperandNumber)}. " +
                         DiscriminantGreaterZhanZeroSecondRoot +
                         $"{(-secondOperandNumber + Math.Sqrt(discriminant)) / (2 * firstOperandNumber)}.";
            }

            return result;
        }
    }
}