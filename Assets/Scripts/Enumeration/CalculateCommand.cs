﻿namespace DefaultNamespace
{
    public enum CalculateCommand
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }
}