using UnityEngine;
using UnityEngine.UI;
using static DefaultNamespace.ComparisonUtil;
using static DefaultNamespace.ParseDoubleUtil;
using static DefaultNamespace.ValidationUtil;

public class TwoNumbersComparator : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private Button compareButton;
    [SerializeField] private Text resultText;

    private void Start()
    {
        compareButton.onClick.AddListener(CompareTwoNumbers);
    }

    private void CompareTwoNumbers()
    {
        GetTrimmedOperandsText(
            out var firstOperandText,
            out var secondOperandText
        );

        var isNotEmptyOperands = ValidateOperandsIsNotEmpty(
            firstOperandText,
            secondOperandText,
            out var emptyOperandsErrorMessage
        );

        if (!isNotEmptyOperands)
        {
            SetResultMessage(emptyOperandsErrorMessage);

            return;
        }

        var isSuccessParsedDoubleFromOperandsText = TryParseDoubleFromOperandsText(
            firstOperandText,
            secondOperandText,
            out var firstOperandNumber,
            out var secondOperandNumber,
            out var failedParseOperandsErrorMessage
        );

        if (!isSuccessParsedDoubleFromOperandsText)
        {
            SetResultMessage(failedParseOperandsErrorMessage);

            return;
        }

        var comparisonExecutionResultMessage = ExecuteComparisonFindBigger(firstOperandNumber, secondOperandNumber);

        SetResultMessage(comparisonExecutionResultMessage);
    }

    private void GetTrimmedOperandsText(out string firstOperandText, out string secondOperandText)
    {
        firstOperandText = firstOperand.text.Trim();
        secondOperandText = secondOperand.text.Trim();
    }

    private void SetResultMessage(string message)
    {
        resultText.text = message;
    }
}