using UnityEngine;
using UnityEngine.UI;
using static DefaultNamespace.ComparisonUtil;
using static DefaultNamespace.ParseDoubleUtil;
using static DefaultNamespace.ValidationUtil;

public class ThreeNumbersComparator : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private InputField thirdOperand;
    [SerializeField] private Button compareButton;
    [SerializeField] private Text resultText;

    private void Start()
    {
        compareButton.onClick.AddListener(CompareThreeNumbers);
    }

    private void CompareThreeNumbers()
    {
        GetTrimmedOperandsText(
            out var firstOperandText,
            out var secondOperandText,
            out var thirdOperandText
        );

        var isNotEmptyOperands = ValidateOperandsIsNotEmpty(
            firstOperandText,
            secondOperandText,
            thirdOperandText,
            out var emptyOperandsErrorMessage
        );

        if (!isNotEmptyOperands)
        {
            SetResultMessage(emptyOperandsErrorMessage);

            return;
        }

        var isSuccessParsedDoubleFromOperandsText = TryParseDoubleFromOperandsText(
            firstOperandText,
            secondOperandText,
            thirdOperandText,
            out var firstOperandNumber,
            out var secondOperandNumber,
            out var thirdOperandNumber,
            out var failedParseOperandsErrorMessage
        );

        if (!isSuccessParsedDoubleFromOperandsText)
        {
            SetResultMessage(failedParseOperandsErrorMessage);

            return;
        }

        var comparisonExecutionResultMessage = ExecuteComparisonFindTwoBiggest(
            firstOperandNumber,
            secondOperandNumber,
            thirdOperandNumber
        );

        SetResultMessage(comparisonExecutionResultMessage);
    }

    private void GetTrimmedOperandsText(
        out string firstOperandText,
        out string secondOperandText,
        out string thirdOperandText
    )
    {
        firstOperandText = firstOperand.text.Trim();
        secondOperandText = secondOperand.text.Trim();
        thirdOperandText = thirdOperand.text.Trim();
    }

    private void SetResultMessage(string message)
    {
        resultText.text = message;
    }
}