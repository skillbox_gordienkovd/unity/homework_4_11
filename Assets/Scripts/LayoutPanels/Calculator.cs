using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;
using static DefaultNamespace.CalculateCommand;
using static DefaultNamespace.CalculationUtil;
using static DefaultNamespace.ParseDoubleUtil;
using static DefaultNamespace.ResultTextMessages;

public class Calculator : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private List<Button> mathOperationButtons;
    [SerializeField] private Text resultText;

    private Dictionary<Button, CalculateCommand> _mathOperationButtonsToCalculateCommands;

    private void Start()
    {
        _mathOperationButtonsToCalculateCommands = new Dictionary<Button, CalculateCommand>()
        {
            {mathOperationButtons[0], Add},
            {mathOperationButtons[1], Subtract},
            {mathOperationButtons[2], Multiply},
            {mathOperationButtons[3], Divide}
        };

        mathOperationButtons.ForEach(button => button.onClick.AddListener(delegate
        {
            Calculate(_mathOperationButtonsToCalculateCommands[button]);
        }));
    }

    private void Calculate(CalculateCommand command)
    {
        GetTrimmedOperandsText(
            out var firstOperandText,
            out var secondOperandText
        );

        var isNotEmptyOperands = ValidationUtil.ValidateOperandsIsNotEmpty(
            firstOperandText,
            secondOperandText,
            out var emptyOperandsErrorMessage
        );

        if (!isNotEmptyOperands)
        {
            SetResultMessage(emptyOperandsErrorMessage);

            return;
        }

        var isSuccessParsedDoubleFromOperandsText = TryParseDoubleFromOperandsText(
            firstOperandText,
            secondOperandText,
            out var firstOperandNumber,
            out var secondOperandNumber,
            out var failedParseOperandsErrorMessage
        );

        if (!isSuccessParsedDoubleFromOperandsText)
        {
            SetResultMessage(failedParseOperandsErrorMessage);

            return;
        }

        if (secondOperandNumber == 0 && command.Equals(Divide))
        {
            SetResultMessage(SecondOperandIsZero);

            return;
        }

        var executeCalculateResultMessage = ExecuteCalculateCommand(command, firstOperandNumber, secondOperandNumber);

        SetResultMessage(executeCalculateResultMessage);
    }

    private void GetTrimmedOperandsText(out string firstOperandText, out string secondOperandText)
    {
        firstOperandText = firstOperand.text.Trim();
        secondOperandText = secondOperand.text.Trim();
    }

    private void SetResultMessage(string message)
    {
        resultText.text = message;
    }
}