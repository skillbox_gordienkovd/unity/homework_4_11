using UnityEngine;
using UnityEngine.UI;
using static DefaultNamespace.CalculationUtil;
using static DefaultNamespace.ParseDoubleUtil;
using static DefaultNamespace.ResultTextMessages;
using static DefaultNamespace.ValidationUtil;

public class SquareEquationSolver : MonoBehaviour
{
    [SerializeField] private InputField firstOperand;
    [SerializeField] private InputField secondOperand;
    [SerializeField] private InputField thirdOperand;
    [SerializeField] private Button solveButton;
    [SerializeField] private Text resultText;

    private void Start()
    {
        solveButton.onClick.AddListener(SolveSquareEquation);
    }

    private void SolveSquareEquation()
    {
        GetTrimmedOperandsText(
            out var firstOperandText,
            out var secondOperandText,
            out var thirdOperandText
        );

        var isNotEmptyOperands = ValidateOperandsIsNotEmpty(
            firstOperandText,
            secondOperandText,
            thirdOperandText,
            out var emptyOperandsErrorMessage
        );

        if (!isNotEmptyOperands)
        {
            SetResultMessage(emptyOperandsErrorMessage);

            return;
        }

        var isSuccessParsedDoubleFromOperandsText = TryParseDoubleFromOperandsText(
            firstOperandText,
            secondOperandText,
            thirdOperandText,
            out var firstOperandNumber,
            out var secondOperandNumber,
            out var thirdOperandNumber,
            out var failedParseOperandsErrorMessage
        );

        if (!isSuccessParsedDoubleFromOperandsText)
        {
            SetResultMessage(failedParseOperandsErrorMessage);

            return;
        }

        if (firstOperandNumber == 0)
        {
            SetResultMessage(FirstOperandIsZero);

            return;
        }

        var comparisonExecutionResultMessage = ExecuteSquareEquationDecision(
            firstOperandNumber,
            secondOperandNumber,
            thirdOperandNumber
        );

        SetResultMessage(comparisonExecutionResultMessage);
    }

    private void GetTrimmedOperandsText(
        out string firstOperandText,
        out string secondOperandText,
        out string thirdOperandText
    )
    {
        firstOperandText = firstOperand.text.Trim();
        secondOperandText = secondOperand.text.Trim();
        thirdOperandText = thirdOperand.text.Trim();
    }

    private void SetResultMessage(string message)
    {
        resultText.text = message;
    }
}